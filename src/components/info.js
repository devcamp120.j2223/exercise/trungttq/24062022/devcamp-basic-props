import { Component } from "react";
class Info extends Component{
    render(){
        let {firstName, lastName, favNumber} = this.props;
        return(
            <div>
                My name id {firstName} {lastName} and my favourite number is {favNumber}
            </div>
        )
    }
}
export default Info